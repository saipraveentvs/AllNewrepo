package com.samsiphone.TestSuiteBase;

import java.awt.Image;
import java.awt.Toolkit;
import java.awt.image.PixelGrabber;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import java.io.FileInputStream;
import java.io.FileOutputStream;

import org.apache.commons.io.FileUtils;
import org.apache.commons.jxpath.ri.model.beans.NullPointer;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.ITestResult;
import org.testng.SkipException;
import org.testng.asserts.SoftAssert;

import com.samsiphone.utility.Read_XLS;
import com.samsiphone.utility.SuiteUtility;



public class Setup {	
	public static  WebDriverWait wait;
	public static Read_XLS TestSuiteListExcel=null;
	public static Read_XLS SmokeTestSuite=null;
	public static Read_XLS RegressionTestSuite=null;
	public static Logger Add_Log = null;
	public boolean BrowseralreadyLoaded=false;
	public static Properties Param = null;
	public static Properties Object = null;
	public static Properties log4jConfPath=null;
	public static WebDriver driver=null;
	public static WebDriver ExistingchromeBrowser;
	public static WebDriver ExistingmozillaBrowser;
	public static WebDriver ExistingIEBrowser;
	public static WebDriver ExistingSafariBrowser;
	Read_XLS FilePath = null;
	String SheetName = null;
	String TestCaseName = null;	
	String TestCaseName2;
	InputStream baseimage = null;
	String ToRunColumnNameTestCase = null;
	static boolean TestCasePass=true;	
	protected static boolean Testskip;
	protected static boolean Testfail;
	protected String ActualResult;
	protected String ExpectedResult;
	String destDir;
	String destFile;
	
	public void initializeex() throws IOException{
		
		//To Initialize logger service.
		/*String log4jConfPath = ("user.dir")+"/src/com/samsiphone/property/log4j.properties";
		PropertyConfigurator.configure(log4jConfPath);*/
		
		log4jConfPath = new Properties();
		FileInputStream fip = new FileInputStream(System.getProperty("user.dir")+"//src//com//samsiphone//property//log4j.properties");
		log4jConfPath.load(fip);
		
				Add_Log = Logger.getLogger("rootLogger");				
						
				//Please change file's path strings bellow If you have stored them at location other than bellow.
				//Initializing Test Suite List(TestSuiteList.xls) File Path Using Constructor Of Read_XLS Utility Class.
				TestSuiteListExcel = new Read_XLS(System.getProperty("user.dir")+"/src/com/samsiphone/ExcelFiles/TestSuiteList.xls");
				//Initializing Test Suite One(SuiteOne.xls) File Path Using Constructor Of Read_XLS Utility Class.
				SmokeTestSuite = new Read_XLS(System.getProperty("user.dir")+"/src/com/samsiphone/ExcelFiles/SmokeTestSuite.xls");
				//Initializing Test Suite Two(SuiteTwo.xls) File Path Using Constructor Of Read_XLS Utility Class.
				RegressionTestSuite = new Read_XLS(System.getProperty("user.dir")+"/src/com/samsiphone/ExcelFiles/RegressionTestSuite.xls");
				//Bellow given syntax will Insert log In applog.log file.
				Add_Log.info("All Excel Files Initialised successfully.");
				
				//Initialize Param.properties file.
				Param = new Properties();
				fip = new FileInputStream(System.getProperty("user.dir")+"//src//com//samsiphone//property//Param.properties");
				Param.load(fip);
				Add_Log.info("Param.properties file loaded successfully.");		
			
				//Initialize Objects.properties file.
				Object = new Properties();
				fip = new FileInputStream(System.getProperty("user.dir")+"//src//com//samsiphone//property//Objects.properties");
				Object.load(fip);
				Add_Log.info("Objects.properties file loaded successfully.");
				
				
				//To set Smoketestsuite.xls file's path In FilePath Variable.
				FilePath = SmokeTestSuite;				
				TestCaseName = this.getClass().getSimpleName();	
				System.out.println(""+TestCaseName);
				//SheetName to check CaseToRun flag against test case.
				SheetName = "TestCasesList";			
				//Name of column In TestCasesList Excel sheet.
				ToRunColumnNameTestCase = "CaseToRun";			
				//Bellow given syntax will Insert log In applog.log file.
				System.out.println("Execution started.");
				//Add_Log.info(TestCaseName+" : Execution started.");			
				//To check test case's CaseToRun = Y or N In related excel sheet.
				//If CaseToRun = N or blank, Test case will skip execution. Else It will be executed.
				System.out.println(""+FilePath +SheetName +ToRunColumnNameTestCase+ TestCaseName );
				if(!SuiteUtility.checkToRunUtility(FilePath, SheetName,ToRunColumnNameTestCase,TestCaseName)){				
					Add_Log.info(TestCaseName+" : CaseToRun = N for So Skipping Execution.");			
					//To report result as skip for test cases In TestCasesList sheet.
					SuiteUtility.WriteResultUtility(FilePath, SheetName, "Pass/Fail/Skip", TestCaseName, "SKIP");
					//To throw skip exception for this test case.
					throw new SkipException(TestCaseName+"'s CaseToRun Flag Is 'N' Or Blank. So Skipping Execution Of "+TestCaseName);
					
				}

				System.out.println("initializeex end");
			}	
		  
		public void startapp() throws IOException{
			 System.out.println("app start");
		  DesiredCapabilities cap = new DesiredCapabilities();
		  System.out.println("app start1");
		 cap.setCapability("platformName", "iOS"); 
		 System.out.println("app start2");
		  cap.setCapability("platformVersion", Param.getProperty("platformVersion"));
		  System.out.println("app start3");
		  cap.setCapability(CapabilityType.BROWSER_NAME, Param.getProperty("BROWSER_NAME"));
		  System.out.println("app start4");
		  cap.setCapability("device", Param.getProperty("device"));
		  System.out.println("app start5");
		  cap.setCapability("deviceName", Param.getProperty("deviceName"));
		  System.out.println("app start6");
		  cap.setCapability("app", Param.getProperty("AppPath"));
		  System.out.println("app start7");
		  driver = new RemoteWebDriver(new URL("http://localhost:4788/wd/hub"), cap);
		  System.out.println("app start go");
		  //wait  = new WebDriverWait(driver, 8);
		   //driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		 /* System.out.println("before load");
			  wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(Object.getProperty("HomeScreenLoad"))));
			  System.out.println("App Launch Success");
				  	
			  try
			  {  	
				  wait.until(ExpectedConditions.alertIsPresent());
			
				  // driver.switchTo().alert().dismiss();  
				   driver.findElement(By.xpath("//UIAApplication[1]/UIAWindow[6]/UIAAlert[1]/UIACollectionView[1]/UIACollectionCell[2]")).click();
				   wait.until(ExpectedConditions.presenceOfElementLocated(By.name("Sign In")));
			  }
			      catch(Exception e){ 
					  
				   System.out.println("unexpected alert not present");   
				   wait.until(ExpectedConditions.presenceOfElementLocated(By.name("Sign In")));
				  }*/
			
	}
		
		public void closeapp(){
		
		
		driver.quit();
		
		System.out.println("*--------------------------------*---------------------------------------*");
		}
		
		public void terminateex(){
	
					if ((ActualResult==null)||(ExpectedResult==null)) {
						 System.out.println("Actual Result or Expected Result is NULL");
						 SuiteUtility.WriteResultUtility(FilePath, SheetName, "Pass/Fail/Skip", TestCaseName, "EXCEPTION");	
					return;
					}
           	
			 else if((ActualResult.equalsIgnoreCase(ExpectedResult))){
						//If expected and actual results not match, Set flag Testfail=true.
						//Testfail=true;
						
						System.out.println("Reporting test case Pass");
						//Set TestCasePass = false to report test case as fail In excel sheet.
						//TestCasePass=false;	
						//If found Testfail = true, Result will be reported as FAIL against data set line In excel sheet.
						SuiteUtility.WriteResultUtility(FilePath, SheetName, "Pass/Fail/Skip", TestCaseName, "PASS");		
	            	}
					
	    
			else if(Testskip){
	            		
	            		System.out.println("Reporting test case Skipped");
						//If found Testskip = true, Result will be reported as SKIP against data set line In excel sheet.
						SuiteUtility.WriteResultUtility(FilePath, SheetName, "Pass/Fail/Skip", TestCaseName, "SKIP");	
					
	            	}
	
	            else{
	            		System.out.println("Reporting test case failed");
						//If found Testskip = false and Testfail = false, Result will be reported as PASS against data set line In excel sheet.
						SuiteUtility.WriteResultUtility(FilePath, SheetName, "Pass/Fail/Skip", TestCaseName, "FAIL");
					
	            	}

		}
				

		//Function to capture screenshot.
		public void captureScreenShot(){	

			//String destDir = "";
			/* class Local {}; String TestCaseName2 = Local.class.getEnclosingMethod().getName(); 	
				System.out.println(""+TestCaseName2);*/
				
				StackTraceElement[] stacktrace = Thread.currentThread().getStackTrace();
			    StackTraceElement e = stacktrace[2];//coz 0th will be getStackTrace so 1st
			    String TestCaseName2 = e.getMethodName();
			    System.out.println("this is method"+TestCaseName2);
			String passfailMethod = TestCaseName + "." + TestCaseName2;
			//To capture screenshot.
			File scrFile = ((TakesScreenshot) Setup.driver).getScreenshotAs(OutputType.FILE);
	    	DateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy__hh_mm_ssaa");
	    	//If status = fail then set folder name "screenshots/Failures"

	    	
	    	 destDir = "screenshots/CurrentRunScreenshots";
	    		

	    	//To create folder to store screenshots
	    	new File(destDir).mkdirs();
	    	//Set file name with combination of test class name + date time.
	    	//String destFile = passfailMethod+" - "+dateFormat.format(new Date()) + ".png";
	    	 destFile = passfailMethod+".png";
	        try {
	        	//Store file at destination folder location
	     	   FileUtils.copyFile(scrFile, new File(destDir + "/" + destFile));
	     	   
	     	//To set Smoketestsuite.xls file's path In FilePath Variable.
	  		//Initializing Test Suite One(SuiteOne.xls) File Path Using Constructor Of Read_XLS Utility Class.

	  				/*
	  				List<String> results1 = new ArrayList<String>();
	  				
	  				File[] files3 = new File("/Users/vpsai/Documents/workspace/SamsClubiPhone/screenshots/CurrentRunScreenshots").listFiles();
	  				//If this pathname does not denote a directory, then listFiles() returns null. 

	  				for (File files : files3) {
	  				    if (files.isFile()) {
	  				        results1.add(files.getName());

	  				    }
	  			      
	  				}
	  				  System.out.println(""+results1);
	  				  String path = results1.toString();
	  				  String value = path.replace("]", "");
	  				  String value2 = value.replace("[", "");
	  				  String findStr = ",";
	  				 int cou = value2.split(findStr).length;
	  				 System.out.println(""+cou);
	  				  String[] out = value2.split(",");*/
	  				  
	  				  
	  					List<String> results2 = new ArrayList<String>();
	  					
	  					File[] files1 = new File("/Users/vpsai/Documents/workspace/SamsClubiPhone/screenshots/BaseScreenshots").listFiles();
	  					//If this pathname does not denote a directory, then listFiles() returns null. 

	  					for (File files2 : files1) {
	  					    if (files2.isFile()) {
	  					        results2.add(files2.getName());

	  					    }
	  				      
	  					}
	  					  System.out.println(""+results2);
	  					  
	  					  String path1 = results2.toString();
	  					  String value1 = path1.replace("]", "");
	  					  String value3 = value1.replace("[", "");
	  					  String findStr1 = ",";
	  					 int cou1 = value3.split(findStr1).length;
	  					 System.out.println(""+cou1);
	  					  String[] out1 = value3.split(",");
	  					 

	  						  for(int j=0;j<cou1;j++)
	  						  {
	  							 System.out.println("TestCaseName"+destFile.trim());
	  							  System.out.println("out1)"+out1[j].trim());
	  						  
	  						  if((destFile.trim()).equals(out1[j].trim()))
	  						  {
	  							  System.out.println("This is equal"+destFile+"="+out1[j].trim());
	  					
	  					  
	  					  
	  					  //System.out.println(""+ results1<String>);
	  					  //System.out.println(""+ results1(1));
	  			
	  			 
	  			try {
	  				  
	  				 
	  				String file1 = "/Users/vpsai/Documents/workspace/SamsClubiPhone/screenshots/BaseScreenshots/"+out1[j].trim();
	  				String file2 = "/Users/vpsai/Documents/workspace/SamsClubiPhone/screenshots/CurrentRunScreenshots/"+destFile.trim();
	  				System.out.print(""+file1);
	  				System.out.print(""+file2);
	  				 
	  				
	  				Image image1 = Toolkit.getDefaultToolkit().getImage(file1);
	  				Image image2 = Toolkit.getDefaultToolkit().getImage(file2);
	  			
	  			PixelGrabber grab1 =new PixelGrabber(image1, 0, 40, -1, -1, false);
	  			System.out.println(""+grab1);
	  			PixelGrabber grab2 =new PixelGrabber(image2, 0, 40, -1, -1, false);
	  			System.out.println(""+grab2);
	  			 
	  			int[] data1 = null;
	  			 
	  			if (grab1.grabPixels()) {
	  			int width = grab1.getWidth();
	  			int height = grab1.getHeight();
	  			data1 = new int[width * height];
	  			data1 = (int[]) grab1.getPixels();
	  			}
	  			 
	  			int[] data2 = null;
	  			 
	  			if (grab2.grabPixels()) {
	  			int width = grab2.getWidth();
	  			int height = grab2.getHeight();
	  			
	  			System.out.println(""+height);
	  			System.out.println(""+width);
	  			data2 = new int[width * height];
	  			data2 = (int[]) grab2.getPixels();
	  			}
	  			

	  			System.out.println("Pixels equal: " + java.util.Arrays.equals(data1, data2));

	  			if(java.util.Arrays.equals(data1, data2)){
	  				
	  				System.out.println("both are equal");
	  				SuiteUtility.WriteResultUtility(FilePath, SheetName, "UiVerfication", TestCaseName, "PASS");	
	  					
	  			}
	  			else
	  			{
	  				System.out.println("both are not equal");
	  				SuiteUtility.WriteResultUtility(FilePath, SheetName, "UiVerfication", TestCaseName, "FAIL");	
	  				//SuiteUtility.WriteResultUtility(FilePath, SheetName, "Pass/Fail/Skip", TestCaseName, "SKIP");
	  				
	  				
	  			}
	  			 
	  			}
	  			
	  			catch (InterruptedException e1) {
	  			e1.printStackTrace();
	  			}
	  			
	  			break;
	  						  }
	  						  else if(cou1==j+1)
	  						  {
	  							System.out.println("PLEASE BASE LINE THIS IMAGE");
	  							
	  							  
	  						  }
	  						  else{
	  							  
	  							  System.out.println("This is not equal"+destFile+"="+out1[j]);
	  						  }
	  						  
	  						  }
	  	  
	     	   
	        }
	        catch (IOException e1) {
	             e1.printStackTrace();
	        }
		}
		
		
		public void Imagewriteexl() throws FileNotFoundException{
			baseimage = new FileInputStream(destDir + "/" + destFile);
			
			SuiteUtility.WriteResultUtility(FilePath, SheetName, "BaseImage", TestCaseName, baseimage);	
			
		
			System.out.println(""+FilePath);
			System.out.println(""+SheetName);
			System.out.println(""+TestCaseName);
			
			System.out.println(""+baseimage);
		}
		
		
		public void loadWebBrowser() throws IOException{
			//Check If any previous webdriver browser Instance Is exist then run new test In that existing webdriver browser Instance.
				if(Param.getProperty("testBrowser").equalsIgnoreCase("Mozilla") && ExistingmozillaBrowser!=null){
					driver = ExistingmozillaBrowser;
					return;
				}else if(Param.getProperty("testBrowser").equalsIgnoreCase("chrome") && ExistingchromeBrowser!=null){
					driver = ExistingchromeBrowser;
					return;
				}else if(Param.getProperty("testBrowser").equalsIgnoreCase("IE") && ExistingIEBrowser!=null){
					driver = ExistingIEBrowser;
					return;
				}		
				else if(Param.getProperty("testBrowser").equalsIgnoreCase("Safari") && ExistingSafariBrowser!=null){
					driver = ExistingSafariBrowser;
					return;
				}
			
				if(Param.getProperty("testBrowser").equalsIgnoreCase("Mozilla")){
					//To Load Firefox driver Instance. 
					driver = new FirefoxDriver();
					ExistingmozillaBrowser=driver;
					Add_Log.info("Firefox Driver Instance loaded successfully.");
					
				}else if(Param.getProperty("testBrowser").equalsIgnoreCase("Chrome")){
					//To Load Chrome driver Instance.
					System.setProperty("webdriver.chrome.driver", "/Applications/Google Chrome.app");
					driver = new ChromeDriver();
					ExistingchromeBrowser=driver;
					Add_Log.info("Chrome Driver Instance loaded successfully.");
				}
					
					else if(Param.getProperty("testBrowser").equalsIgnoreCase("Safari")){
						//To Load Safari.
						DesiredCapabilities cap = new DesiredCapabilities();
						   cap.setCapability("deviceName", Param.getProperty("deviceName"));
					       cap.setCapability("browsername", Param.getProperty("browsername"));
					       cap.setCapability("platformVersion", Param.getProperty("platformVersion"));
					       cap.setCapability("platformName", Param.getProperty("platformName"));
					       cap.setCapability("interKeyDelay", 0);
							  driver = new RemoteWebDriver(new URL("http://localhost:4723/wd/hub"), cap);
					       ExistingSafariBrowser=driver;
							Add_Log.info("Safari Instance loaded successfully.");
					
				}else if(Param.getProperty("testBrowser").equalsIgnoreCase("IE")){
					//To Load IE driver Instance.
					System.setProperty("webdriver.ie.driver", System.getProperty("user.dir")+"//BrowserDrivers//IEDriverServer.exe");
					driver = new InternetExplorerDriver();
					ExistingIEBrowser=driver;
					Add_Log.info("IE Driver Instance loaded successfully.");
					
				}			
				driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				driver.manage().window().maximize();			
		}
		
		public void closeWebBrowser(){
			driver.close();
			//null browser Instance when close.
			ExistingchromeBrowser=null;
			ExistingmozillaBrowser=null;
			ExistingIEBrowser=null;
			ExistingSafariBrowser=null;
		}
		


		
	
	//getElementByXPath function for static xpath
	public WebElement getElementByXPath(String Key){
		try{
			//This block will find element using Key value from web page and return It.
			return driver.findElement(By.xpath(Object.getProperty(Key)));
		}catch(Throwable t){
			//If element not found on page then It will return null.
			Add_Log.debug("Object not found for key --"+Key);
			return null;
		}
	}
	
	//getElementByXPath function for dynamic xpath
	public WebElement getElementByXPath(String Key1, int val, String key2){
		try{
			//This block will find element using values of Key1, val and key2 from web page and return It.
			return driver.findElement(By.xpath(Object.getProperty(Key1)+val+Object.getProperty(key2)));
		}catch(Throwable t){
			//If element not found on page then It will return null.
			Add_Log.debug("Object not found for custom xpath");
			return null;
		}
	}
	
	//Call this function to locate element by ID locator.
	public WebElement getElementByID(String Key){
		try{
			return driver.findElement(By.id(Object.getProperty(Key)));
		}catch(Throwable t){
			Add_Log.debug("Object not found for key --"+Key);
			return null;
		}
	}
	
	//Call this function to locate element by Name Locator.
	public WebElement getElementByName(String Key){
		try{
			return driver.findElement(By.name(Object.getProperty(Key)));
		}catch(Throwable t){
			Add_Log.debug("Object not found for key --"+Key);
			return null;
		}
	}
	
	//Call this function to locate element by cssSelector Locator.
	public WebElement getElementByCSS(String Key){
		try{
			return driver.findElement(By.cssSelector(Object.getProperty(Key)));
		}catch(Throwable t){
			Add_Log.debug("Object not found for key --"+Key);
			return null;
		}
	}
	
	//Call this function to locate element by ClassName Locator.
	public WebElement getElementByClass(String Key){
		try{
			return driver.findElement(By.className(Object.getProperty(Key)));
		}catch(Throwable t){
			Add_Log.debug("Object not found for key --"+Key);
			return null;
		}
	}
	
	//Call this function to locate element by tagName Locator.
	public WebElement getElementByTagName(String Key){
		try{
			return driver.findElement(By.tagName(Object.getProperty(Key)));
		}catch(Throwable t){
			Add_Log.debug("Object not found for key --"+Key);
			return null;
		}
	}
	
	//Call this function to locate element by link text Locator.
	public WebElement getElementBylinkText(String Key){
		try{
			return driver.findElement(By.linkText(Object.getProperty(Key)));
		}catch(Throwable t){
			Add_Log.debug("Object not found for key --"+Key);
			return null;
		}
	}
	
	//Call this function to locate element by partial link text Locator.
	public WebElement getElementBypLinkText(String Key){
		try{
			return driver.findElement(By.partialLinkText(Object.getProperty(Key)));
		}catch(Throwable t){
			Add_Log.debug("Object not found for key --"+Key);
			return null;
		}
	}
}


package com.samsiphone.SmokeTest;


import java.io.IOException;

import org.openqa.selenium.By;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import io.appium.java_client.ios.*;
import io.appium.java_client.*;
import io.appium.*;

import com.samsiphone.TestSuiteBase.Setup;


public class HomeScreen extends SmokeTestBase {
	
	
	@BeforeTest
	
	public void before() throws IOException{
	
		//Called init() function from SuiteBase class to Initialize .xls Files
		initializeex();	
		startapp();
						}	

	
	@AfterTest
	
	
	public void after() throws IOException{
		
		terminateex();
		
		
		closeapp();
	

	}
	
	
  @Test	  
	  public void NotSigned() throws InterruptedException, IOException {
	
	
		
		System.out.println("My Test start");
		// driver.findElement(By.tagName("Sign In")).click();
		
		getElementByXPath("SignInButton_Home").click();
		
		  //driver.findElement(By.xpath(Object.getProperty("SignInButton_Home"))).click();
		  
		  System.out.println("Sigin click");
		  
		   ActualResult = getElementByXPath("SignInButton_Login").getText();
		  
		  System.out.println(""+ActualResult);
		   ExpectedResult = "Sign In Button";
		   captureScreenShot();
		  // Imagewriteexl();
	
	       }
	
}
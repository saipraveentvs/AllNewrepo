package com.samsiphone.SmokeTest;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.AfterClass;

public class Checkout extends SmokeTestBase {
  @Test
  public void logincheckout() {
	   driver.findElement(By.xpath(Object.getProperty("Search"))).click();
		 System.out.println("search bar up");
		 //driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		 System.out.println("search waite");
		 driver.findElement(By.xpath("//UIAApplication[1]/UIAWindow[3]/UIAKeyboard[1]/UIAKey[28]")).click();
		 driver.findElement(By.xpath("//UIAApplication[1]/UIAWindow[3]/UIAKeyboard[1]/UIAKey[4]")).click();
		 driver.findElement(By.xpath("//UIAApplication[1]/UIAWindow[3]/UIAKeyboard[1]/UIAKey[1]")).click();
		 driver.findElement(By.xpath("//UIAApplication[1]/UIAWindow[3]/UIAKeyboard[1]/UIAKey[6]")).click();
		 driver.findElement(By.xpath("//UIAApplication[1]/UIAWindow[3]/UIAKeyboard[1]/UIAKey[10]")).click();
		 driver.findElement(By.xpath("//UIAApplication[1]/UIAWindow[3]/UIAKeyboard[1]/UIAKey[9]")).click();
		 driver.findElement(By.xpath("//UIAApplication[1]/UIAWindow[3]/UIAKeyboard[1]/UIAButton[4]")).click();
	 // getElementByXPath("Search_bar").sendKeys("41609");
			  //driver.findElement(By.xpath(Object.getProperty("Search"))).
			  System.out.println("send keys success");
			  wait.until(ExpectedConditions.presenceOfElementLocated(By.name("quick add button")));	
			  getElementByXPath("Shelf_1").click();
			  wait.until(ExpectedConditions.presenceOfElementLocated(By.name("Item Details")));
	  getElementByXPath("ship_this_item").click();
		getElementByXPath("add_to_cart").click();
		getElementByXPath("cart_icon_itemdetails").click();
		wait.until(ExpectedConditions.presenceOfElementLocated(By.name("Edit")));	
		getElementByXPath("Begin_checkout").click();
		wait.until(ExpectedConditions.presenceOfElementLocated(By.name("Payment Method")));
		getElementByXPath("Place_order").click();
		
	  
	  
	  
  }
  @BeforeClass
  public void beforeClass() throws IOException {
	  initializeex();	
		startapp();
		
		getElementByXPath("carticon").click();
		System.out.println("carticon click");
		
		driver.manage().timeouts().implicitlyWait(8, TimeUnit.SECONDS);
		System.out.println("carticon waite");
				
		Boolean iselementpresent = driver.findElements(By.xpath("//UIAApplication[1]/UIAWindow[2]/UIATableView[1]/UIATableCell[1]/UIAButton[1]")).size()!= 0;
		   if (iselementpresent == true){

			getElementByXPath("Cntshipping").click();
			logincheckout();
		
		}
		else{
				
			driver.findElement(By.name("Edit")).click();
			driver.findElement(By.xpath("//UIAApplication[1]/UIAWindow[2]/UIATableView[1]/UIATableCell[2]/UIAButton[1]")).click();
			wait.until(ExpectedConditions.presenceOfElementLocated(By.name("Payment Method")));
			driver.findElement(By.name("Empty Cart")).click();
			driver.switchTo().alert().accept();
			//wait.until(ExpectedConditions.presenceOfElementLocated(By.name("Remove all items from your Cart?")));
			getElementByXPath("cart_close").click();
			logincheckout();
		}
		
		
		
  }

  @AfterClass
  public void afterClass() {
	  terminateex();
		closeapp();
	
  }

}

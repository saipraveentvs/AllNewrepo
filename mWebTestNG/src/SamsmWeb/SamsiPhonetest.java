package SamsmWeb;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.Test;

public class SamsiPhonetest {
  @Test
  public void f() throws InterruptedException, IOException {
	  WebDriver driver;
	  
	  DesiredCapabilities cap = new DesiredCapabilities();
	  cap.setCapability("platformName", "ios");
	  cap.setCapability("browser", "safari");
	  cap.setCapability("platformVersion", "8.1");
	  cap.setCapability("deviceName", "iPhone 5s");
	 // cap.setCapability("app","/Users/vpsai/Library/Developer/Xcode/DerivedData/UICatalog-cfsnfsqbqopuywfrrmfesplfcpfb/Build/Products/Debug-iphonesimulator/UICatalog.app");
	  cap.setCapability("app","/Users/vpsai/Library/Developer/Xcode/DerivedData/SamsClub-cteovnimjozdsobfjhhjukjbtomc/Build/Products/Debug-iphonesimulator/SamsClub-iPhone.app");
	  
	  driver = new RemoteWebDriver(new URL("http://localhost:4723/wd/hub"), cap);
	  //driver.findElement(By.name("Don’t Allow")).click();
	  //driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	  driver.findElement(By.name("Shop by Category")).click();
		//driver.findElement(By.xpath("//UIAApplication[1]/UIAWindow[1]/UIATableView[1]/UIATableCell[1]/UIAStaticText[1]")).click();
		//driver.findElement(By.xpath("//UIAApplication[1]/UIAWindow[2]/UIATableView[1]/UIATableCell[3]/UIAButton[1]")).click();
		
		File screenshot = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
				FileUtils.copyFile(screenshot,new File("/Users/vpsai/Desktop/Selenium/"+GetTimeStampValue()+".png"));

			driver.quit();
		
  }
  
	public String GetTimeStampValue()throws IOException{

        Calendar cal = Calendar.getInstance();       
         java.util.Date time=cal.getTime();
         String timestamp=time.toString();
            System.out.println(timestamp);
            String systime=timestamp.replace(":", "-");
            System.out.println(systime);
        return systime;

       }
}

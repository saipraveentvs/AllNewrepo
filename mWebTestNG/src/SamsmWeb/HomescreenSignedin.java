package SamsmWeb;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Date;
import java.util.Calendar;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.AfterSuite;

public class HomescreenSignedin {
	WebDriver driver;
	FileInputStream objfile;
	Properties obj;
	
	//File screenshotFile=((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);

	

	public String GetTimeStampValue()throws IOException{

	        Calendar cal = Calendar.getInstance();       
	         java.util.Date time=cal.getTime();
	         String timestamp=time.toString();
	            System.out.println(timestamp);
	            String systime=timestamp.replace(":", "-");
	            System.out.println(systime);
	        return systime;

	       }
	
  @Test
  public void homescreentest() throws IOException{
	  
	  
	  driver.get(obj.getProperty("URL_ENV"));
	  
	  File screenshot = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
	 // FileUtils.copyFile(screenshot, new File("/Users/vpsai/Desktop/Selenium/screenshot.jpg"));	
	  FileUtils.copyFile(screenshot,new File("/Users/vpsai/Desktop/Selenium/"+GetTimeStampValue()+".png")); 
			
		try{
			WebDriverWait wait = new WebDriverWait(driver, 25);
			
	
		  wait.until(ExpectedConditions.textToBePresentInElementLocated(By.cssSelector(obj.getProperty("css_becomemember")), "Just $45 for a year of exclusive savings."));
			System.out.println("Test start1");
		driver.findElement(By.xpath(obj.getProperty("xp_signin"))).click();
		  wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("pLR10"), "Sign In to your Sam's Club account"));
		System.out.println("Test start2");
		driver.findElement(By.cssSelector(obj.getProperty("css_user"))).sendKeys(obj.getProperty("username"));
		System.out.println("Test start3");
		driver.findElement(By.cssSelector(obj.getProperty("css_password"))).sendKeys(obj.getProperty("password"));
	
		driver.findElement(By.cssSelector(obj.getProperty("css_signin_butt"))).click();
		wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("heading"), "Instant Savings*"));

			System.out.println("Test is pass");
		
		}
		
		catch(Exception e){
			System.out.println("Test is fail");
			System.out.println("exception"+e);
			
			
		}
			
			
	  
  }





@BeforeClass
  public void beforeClass() throws IOException {
	  
	  DesiredCapabilities cap = new DesiredCapabilities();
	  cap.setCapability("platformName", "ios");
	  cap.setCapability("browser", "safari");
	  cap.setCapability("platformVersion", "7.1");
	  cap.setCapability("deviceName", "iphone simulator");
		driver = new RemoteWebDriver(new URL("http://localhost:4723/wd/hub"), cap);
	
		
		 obj = new Properties();
		 objfile =new FileInputStream(System.getProperty("user.dir")+"/src/samsclub.properties");
	obj.load(objfile);
	
	}
	
	  
	  
  

  @AfterClass
  public void afterClass() {
	  driver.quit();
  }

}

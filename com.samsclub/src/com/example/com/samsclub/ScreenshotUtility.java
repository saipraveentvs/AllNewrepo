//Find More Tutorials On WebDriver at -> http://software-testing-tutorials-automation.blogspot.com
package com.example.com.samsclub;

import java.awt.Image;
import java.awt.Toolkit;
import java.awt.image.PixelGrabber;
import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import com.samsiphone.TestSuiteBase.Setup;
import com.samsiphone.utility.Read_XLS;
import com.samsiphone.utility.SuiteUtility;




public class ScreenshotUtility implements ITestListener{	
	
	public static Read_XLS SmokeTestSuite=null;
	Read_XLS FilePath = null;
	String SheetName = null;
	String TestCaseName = null;	
	
	
	
	//This method will execute before starting of Test suite.
	public void onStart(ITestContext tr) {	
		
	}

	//This method will execute, Once the Test suite is finished.
	public void onFinish(ITestContext tr) {
		
		
	    	
		
	}
	

	//This method will execute only when the test is pass.
	public void onTestSuccess(ITestResult tr) {
		//If screenShotOnPass = yes, call captureScreenShot.
		if(Setup.Param.getProperty("screenShotOnPass").equalsIgnoreCase("yes"))
		{
			captureScreenShot(tr,"pass");
		}
	}

	//This method will execute only on the event of fail test.
	public void onTestFailure(ITestResult tr) {		
		//If screenShotOnFail = yes, call captureScreenShot.
		if(Setup.Param.getProperty("screenShotOnFail").equalsIgnoreCase("yes"))
		{
			captureScreenShot(tr,"fail");
		}
	}

	// This method will execute before the main test start (@Test)
	public void onTestStart(ITestResult tr) {
		
	}

	// This method will execute only if any of the main test(@Test) get skipped
	public void onTestSkipped(ITestResult tr) {		
	}
	
	public void onTestFailedButWithinSuccessPercentage(ITestResult tr) {
	}
	
	//Function to capture screenshot.
	public void captureScreenShot(ITestResult result, String status){	

		String destDir = "";
		String passfailMethod = result.getMethod().getRealClass().getSimpleName() + "." + result.getMethod().getMethodName();
		//To capture screenshot.
		File scrFile = ((TakesScreenshot) Setup.driver).getScreenshotAs(OutputType.FILE);
    	DateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy__hh_mm_ssaa");
    	//If status = fail then set folder name "screenshots/Failures"

    		destDir = "screenshots/CurrentRunScreenshots";
    		

    	//To create folder to store screenshots
    	new File(destDir).mkdirs();
    	//Set file name with combination of test class name + date time.
    	//String destFile = passfailMethod+" - "+dateFormat.format(new Date()) + ".png";
    	String destFile = passfailMethod+".png";
        try {
        	//Store file at destination folder location
     	   FileUtils.copyFile(scrFile, new File(destDir + "/" + destFile));
     	   
     	   
     	   
     	SmokeTestSuite = new Read_XLS(System.getProperty("user.dir")+"/src/com/samsiphone/ExcelFiles/SmokeTestSuite.xls");
  		FilePath = SmokeTestSuite;		
  		SheetName = "TestCasesList";
  		TestCaseName = result.getMethod().getRealClass().getSimpleName();
  		System.out.println(""+TestCaseName);
  				
  				//To set Smoketestsuite.xls file's path In FilePath Variable.
  		//Initializing Test Suite One(SuiteOne.xls) File Path Using Constructor Of Read_XLS Utility Class.

  				
  				List<String> results1 = new ArrayList<String>();
  				
  				File[] files3 = new File("/Users/vpsai/Documents/workspace/SamsClubiPhone/screenshots/CurrentRunScreenshots").listFiles();
  				//If this pathname does not denote a directory, then listFiles() returns null. 

  				for (File files : files3) {
  				    if (files.isFile()) {
  				        results1.add(files.getName());

  				    }
  			      
  				}
  				  System.out.println(""+results1);
  				  String path = results1.toString();
  				  String value = path.replace("]", "");
  				  String value2 = value.replace("[", "");
  				  String findStr = ",";
  				 int cou = value2.split(findStr).length;
  				 System.out.println(""+cou);
  				  String[] out = value2.split(",");
  				  
  				  
  					List<String> results2 = new ArrayList<String>();
  					
  					File[] files1 = new File("/Users/vpsai/Documents/workspace/SamsClubiPhone/screenshots/BaseScreenshots").listFiles();
  					//If this pathname does not denote a directory, then listFiles() returns null. 

  					for (File files2 : files1) {
  					    if (files2.isFile()) {
  					        results2.add(files2.getName());

  					    }
  				      
  					}
  					  System.out.println(""+results2);
  					  
  					  String path1 = results2.toString();
  					  String value1 = path1.replace("]", "");
  					  String value3 = value1.replace("[", "");
  					  String findStr1 = ",";
  					 int cou1 = value3.split(findStr1).length;
  					 System.out.println(""+cou1);
  					  String[] out1 = value3.split(",");
  					  
  					  for(int i=0;i<cou;i++)
  					  {
  						  for(int j=0;j<cou1;j++)
  						  {
  							  
  						  
  						  if((out[i].trim()).equals(out1[j].trim())){
  							  System.out.println("This is equal"+out1[j].trim()+"="+out[i].trim());
  					
  					  
  					  
  					  //System.out.println(""+ results1<String>);
  					  //System.out.println(""+ results1(1));
  			
  			 
  			try {
  				  
  				 
  				String file1 = "/Users/vpsai/Documents/workspace/SamsClubiPhone/screenshots/BaseScreenshots/"+out1[j].trim();
  				String file2 = "/Users/vpsai/Documents/workspace/SamsClubiPhone/screenshots/CurrentRunScreenshots/"+out[i].trim();
  				System.out.print(""+file1);
  				System.out.print(""+file2);
  				 
  				
  				Image image1 = Toolkit.getDefaultToolkit().getImage(file1);
  				Image image2 = Toolkit.getDefaultToolkit().getImage(file2);
  			
  			PixelGrabber grab1 =new PixelGrabber(image1, 0, 40, -1, -1, false);
  			System.out.println(""+grab1);
  			PixelGrabber grab2 =new PixelGrabber(image2, 0, 40, -1, -1, false);
  			System.out.println(""+grab2);
  			 
  			int[] data1 = null;
  			 
  			if (grab1.grabPixels()) {
  			int width = grab1.getWidth();
  			int height = grab1.getHeight();
  			data1 = new int[width * height];
  			data1 = (int[]) grab1.getPixels();
  			}
  			 
  			int[] data2 = null;
  			 
  			if (grab2.grabPixels()) {
  			int width = grab2.getWidth();
  			int height = grab2.getHeight();
  			
  			System.out.println(""+height);
  			System.out.println(""+width);
  			data2 = new int[width * height];
  			data2 = (int[]) grab2.getPixels();
  			}
  			

  			System.out.println("Pixels equal: " + java.util.Arrays.equals(data1, data2));

  			if(java.util.Arrays.equals(data1, data2)){
  				
  				System.out.println("both are equal");
  				SuiteUtility.WriteResultUtility(FilePath, SheetName, "UiVerfication", TestCaseName, "PASS");	
  					
  			}
  			else
  			{
  				System.out.println("both are not equal");
  				SuiteUtility.WriteResultUtility(FilePath, SheetName, "UiVerfication", TestCaseName, "FAIL");	
  				//SuiteUtility.WriteResultUtility(FilePath, SheetName, "Pass/Fail/Skip", TestCaseName, "SKIP");
  				
  				
  			}
  			 
  			} catch (InterruptedException e1) {
  			e1.printStackTrace();
  			}
  			}
  						  else
  						  {
  							  System.out.println("This is not equal"+out1[j]+"!="+out[i]);
  							  
  							  
  						  }
  						  }
  					  }
  	  
     	   
        }
        catch (IOException e) {
             e.printStackTrace();
        }
   } 
}
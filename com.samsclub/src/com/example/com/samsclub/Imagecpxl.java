package com.example.com.samsclub;

import java.awt.Image;
import java.awt.Toolkit;

import com.samsiphone.TestSuiteBase.Setup;
import com.samsiphone.utility.Read_XLS;
import com.samsiphone.utility.SuiteUtility;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
 
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.ss.usermodel.ClientAnchor;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Drawing;
import org.apache.poi.ss.usermodel.Picture;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.util.IOUtils;

public class Imagecpxl{
	
	public static void main(String args[]){
		copyimgxl();
	}

	private static void copyimgxl() {
		// TODO Auto-generated method stub
	/*	Image Imagepath;

		String SheetName = "TestCasesList";
		String TestCaseName ="HomeScreenTest";	
		Read_XLS SmokeTestSuite = new Read_XLS(System.getProperty("user.dir")+"/src/com/samsiphone/ExcelFiles/SmokeTestSuite.xls");
		//Initializing Test Suite Two(SuiteTwo.xls) File Path Using Constructor Of Read_XLS Utility Class.
		Read_XLS FilePath = SmokeTestSuite;	
		
		Imagepath = Toolkit.getDefaultToolkit().getImage("/Users/vpsai/Desktop/scrFilegrab1.png");
		System.out.println(""+Imagepath);
		System.out.println(""+SmokeTestSuite);
		SuiteUtility.WriteResultUtility(FilePath, SheetName, "BaseImage", TestCaseName, "PASS");	*/
		
		 
		 
		  try {
		 
		   Workbook wb = new XSSFWorkbook();
		   Sheet sheet = wb.createSheet("My Sample Excel");
		 
		   //FileInputStream obtains input bytes from the image file
		   InputStream inputStream = new FileInputStream("/Users/vpsai/Desktop/scrFilegrab1.png");
		   //Get the contents of an InputStream as a byte[].
		   byte[] bytes = IOUtils.toByteArray(inputStream);
		   //Adds a picture to the workbook
		   int pictureIdx = wb.addPicture(bytes, Workbook.PICTURE_TYPE_PNG);
		   //close the input stream
		   inputStream.close();
		 
		   //Returns an object that handles instantiating concrete classes
		   CreationHelper helper = wb.getCreationHelper();
		 
		   //Creates the top-level drawing patriarch.
		   Drawing drawing = sheet.createDrawingPatriarch();
		 
		   //Create an anchor that is attached to the worksheet
		   ClientAnchor anchor = helper.createClientAnchor();
		   //set top-left corner for the image
		   anchor.setCol1(10);
		   anchor.setRow1(20);
		 
		   //Creates a picture
		   Picture pict = drawing.createPicture(anchor, pictureIdx);
		   //Reset the image to the original size
		   pict.resize();
		 
		   //Write the Excel file
		   FileOutputStream fileOut = null;
		   fileOut = new FileOutputStream("/Users/vpsai/Desktop/myFile.xlsx");
		   wb.write(fileOut);
		   fileOut.close();
		 
		  }
		  catch (Exception e) {
		   System.out.println(e);
		  }
		 
		 
		 }
		 
		}



var wd = require("wd"); //Paralle testing //http://sauceio.com/index.php/2012/07/javascript-testing-in-parallel-with-wd-js-and-selenium/

require('colors'); // for console colors
var chai = require("chai"); // 
var chaiAsPromised = require("chai-as-promised");
chai.use(chaiAsPromised);
var should = chai.should();
chaiAsPromised.transferPromiseness = wd.transferPromiseness;

exports.should = should;

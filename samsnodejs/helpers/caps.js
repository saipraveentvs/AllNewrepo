 
exports.ios81 = {
  browserName: '',
  'appium-version': '1.3.4',
  platformName: 'iOS',
  platformVersion: '8.1',
  deviceName: 'iPhone 5s',
  app: undefined // will be set later
};

exports.ios71 = {
  browserName: '',
  'appium-version': '1.3.4',
  platformName: 'iOS',
  platformVersion: '7.1.2',
  deviceName: 'iPhone 5',
  udid:'47c3097a984400bf694ca963842f662d548fdd6f',
  app: undefined // will be set later
};


exports.android18 = {
  browserName: '',
  'appium-version': '1.3.4',
  platformName: 'Android',
  platformVersion: '4.3',
  deviceName: 'Android Emulator',
  app: undefined // will be set later
};

exports.android19 = {
  browserName: '',
  'appium-version': '1.3.3',
  platformName: 'Android',
  platformVersion: '4.4.2',
  deviceName: 'Android Emulator',
  app: undefined // will be set later
};

exports.selendroid16 = {
  browserName: '',
  'appium-version': '1.3.3',
  platformName: 'Android',
  platformVersion: '4.1',
  automationName: 'selendroid',
  deviceName: 'Android Emulator',
  app: undefined // will be set later
};

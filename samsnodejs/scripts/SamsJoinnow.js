'use strict';

var Config = require('config'),
    Testdata = require('./helpers/testdata.js'),
    SigninModule = require('./module/signinModule'),
    AppiumDriver = require('../../../common/common');


describe('Launch app and login tests', function() {

    var appiumDriver = null,
        cap = Config.capabilities.ios81,
        iosVersion = cap.platformVersion;

    cap.app = Config.iosWalmartApp;

    before(function() {
        appiumDriver = new AppiumDriver(Config.localServer);
        appiumDriver.bindModule(SigninModule);
        // require('../../../config/logging').configure(appiumDriver);
    });

    beforeEach(function() {
        return appiumDriver.init(cap);
    });

    afterEach(function() {
        return appiumDriver.quit();
    });

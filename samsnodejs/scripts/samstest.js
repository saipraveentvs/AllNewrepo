"use strict";

require("./helpers/setup");

var wd = require("wd"),
    _ = require('underscore'),
    Q = require('q'),
    serverConfigs = require('./helpers/appium-servers');

describe("samstest", function () {
  this.timeout(300000);
  var driver;
  var allPassed = true;

  before(function () {
    var serverConfig = process.env.SAUCE ?
      serverConfigs.sauce : serverConfigs.local;
    driver = wd.promiseChainRemote(serverConfig);
    require("./helpers/logging").configure(driver);

    var desired = _.clone(require("./helpers/caps").ios81);
    desired.app = require("./helpers/apps").iosTestApp;
    if (process.env.SAUCE) {
      desired.name = 'samstest';
      desired.tags = ['test1'];
    }
    return driver.init(desired);
  });

  after(function () {
    return driver
      .quit()
      .finally(function () {
        if (process.env.SAUCE) {
          return driver.sauceJobStatus(allPassed);
        }
      });
  });

  afterEach(function () {
    allPassed = allPassed && this.currentTest.state === 'passed';
  });



  it("tap on signin", function () {
	   console.log('Start');
   	 		return driver
	 	  	  .sleep(1000) 
		 	  .elementByName('Don’t Allow').click()
			  .sleep(1000)  
	   		  .elementByName('Join Now').click()
    		  .sleep(1000)
	   
   		   /*.resolve(populate()).then(function (sum) {
        return driver.
          elementByAccessibilityId('ComputeSumButton')
            .click().sleep(1000)
          .elementByIosUIAutomation('.elements().withName("Answer");')
            .text().should.become("" + sum);
      });*/
  });

});


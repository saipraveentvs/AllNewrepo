package mytestpack;

import java.io.File;
import java.io.IOException;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;


public class Navigate {
	static WebDriver driver = new FirefoxDriver();

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		
		driver.navigate().to("http://m.samsclub.com/");
		driver.manage().window().maximize();
		
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Test
	public void test() throws IOException {
		driver.findElement(By.xpath(".//*[@id='subMenuList']/ul/li[1]/div/a")).click();
		driver.navigate().back();
		driver.navigate().forward();
		 Set<String> AllWindowHandles = driver.getWindowHandles();
		 String window1 = (String) AllWindowHandles.toArray()[0];
		
		String path = ".//*[@id='lastCrumb']";
		Boolean isElement = driver.findElement(By.xpath(path)).equals(0);
		System.out.println(""+isElement);
		if (isElement == false){
			System.out.println("Element is availanble");
			 //Capture entire page screenshot and then store it to destination drive
			  File screenshot = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
			  FileUtils.copyFile(screenshot, new File("/Users/vpsai/Desktop/Selenium/secreen.jpg"));
			  System.out.print("Capture done");
			 
		}
		else{
			System.out.println("Element is not availanble");
			
		}
			
		
		
		//fail("Not yet implemented");
	}

}

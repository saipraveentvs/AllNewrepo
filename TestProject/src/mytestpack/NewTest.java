package mytestpack;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.util.Properties;

import jxl.Cell;
import jxl.CellType;
import jxl.JXLException;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

public class NewTest {
	
	WebDriver driver;
	FileInputStream objfile;
	Properties obj;
	  String title;
	
	@BeforeClass
	  public void beforeClass() throws IOException, JXLException {
		  
		  DesiredCapabilities cap = new DesiredCapabilities();
		  cap.setCapability("platformName", "ios");
		  cap.setCapability("browser", "safari");
		  cap.setCapability("platformVersion", "7.1");
		  cap.setCapability("deviceName", "iphone simulator");
			driver = new RemoteWebDriver(new URL("http://localhost:4723/wd/hub"), cap);
		
			
			 obj = new Properties();
			 objfile =new FileInputStream(System.getProperty("user.dir")+"/src/samsclub.properties");
		obj.load(objfile);


		}

	
	private String inputFile;

	  public void setInputFile(String inputFile) {
	    this.inputFile = inputFile;
	  }

	  public void read() throws IOException, RowsExceededException, WriteException, BiffException  {
	    File inputWorkbook = new File(inputFile);
	   

	    Workbook w = Workbook.getWorkbook(inputWorkbook);
	      // Get the first sheet
	      WritableSheet sheet = (WritableSheet) w.getSheet(0);
	      // Loop over first 10 column and lines
	 Label label = new Label(1, 1, title);
	 sheet.addCell(label);

		w.close();

	      
	  }

	  public static void main(String[] args) throws IOException {
		  NewTest test = new NewTest();
	    test.setInputFile("/Users/vpsai/Desktop/Selenium/MyDataSheet.xls");
	    try {
			test.read();
		} catch (RowsExceededException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (WriteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (BiffException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	  }
	  
		
	  @Test
	  public void homescreentest(){
		  
		  driver.get(obj.getProperty("URL_ENV"));
		   title = driver.getTitle();
	  }
	  

	  @AfterClass
	  public void afterClass() throws RowsExceededException, WriteException {
		
				
				  driver.quit();
		

}
}
	

package samclubmweb;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Search {
	static WebDriver driver;
   
	//static  WebDriver driver = new AndroidDriver(getActivity()); 

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		
		   
	    
		   DesiredCapabilities cap = new DesiredCapabilities();
	        cap.setCapability("deviceName", "iPhone Simulator");
	        cap.setCapability("browsername", "safari");
	        cap.setCapability("platformVersion", "7.1");
	        cap.setCapability("platformName", "iOS");

	         driver = new RemoteWebDriver(new URL("http://localhost:4723/wd/hub"), cap);
	        driver.get("http://m.samsclub.com");

		    //caps.setCapability("app-package", "com.myapp.test");
		 
		/* caps.setCapability(CapabilityType.BROWSER_NAME, "");
		 caps.setCapability(CapabilityType.VERSION, "4.1.2");
		 caps.setCapability("deviceName", "nexus5");
		 caps.setCapability(CapabilityType.PLATFORM, "Mac");*/
		   
		    
		   // caps.setCapability("device", "Android");
		    //caps.setCapability("platformName", "Android");
		    //caps.setCapability("browserName", "");
		   // caps.setCapability("app-activity", "com.myapp.SplashActivity");
		    //caps.setCapability("takesScreenshot", true);
		    //caps.setCapability("version", "4.4.4");
		   // caps.setCapability("device ID", "064965c90acc796f");
		    //caps.SetCapability("app", @"C:\path to\app\on\pc\app.apk");

		     
		  
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		driver.quit();

	}

	@Test
	public void test() throws InterruptedException, IOException {
		
		//Create Object of Properties Class.
		  Properties obj = new Properties();   
		  //Create Object of FileInputStream Class. Pass file path.
		  FileInputStream objfile = new FileInputStream(System.getProperty("user.dir")+"/src/samclubmweb/samsclub.properties");
		  //Pass object reference objfile to load method of Properties object.
		  obj.load(objfile); 
		  
		
		
		driver.findElement(By.xpath(obj.getProperty("searchbox"))).sendKeys("xbox");
		driver.findElement(By.id("searchButton")).click();
		WebDriverWait wait = new WebDriverWait(driver, 15);
		
		wait.until(ExpectedConditions.textToBePresentInElementLocated(By.xpath(".//*[@id='productList']/li[1]/a/span/span"), "Xbox One Console - Forza 5 Token Bundle"));
		
		

if (driver.findElements(By.id("productList")) != null){
	
	System.out.println("Search results screen open");
		
		
		
	}

else
{
System.out.println("Test fail");	
}
	
		//driver.findElement(By.name("searchParameter"));

		//fail("Not yet implemented");

	
		
		
	
	}
	@Test(timeout=2000)
	public void test2(){
		
		String a = driver.findElement(By.xpath(".//*[@id='productList']/li[1]/a/span/span")).getText();

		System.out.println("This is xpath" +a);
		System.out.println("Test is Pass");
	
	
	
	}
	
	
}
	
	



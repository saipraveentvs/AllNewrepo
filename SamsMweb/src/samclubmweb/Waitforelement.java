package samclubmweb;

import static org.junit.Assert.*;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Waitforelement {
	static WebDriver driver= new FirefoxDriver();
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	
	driver.manage().window().maximize();
	driver.get("http://m.samsclub.com/");

	
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Test
	public void test() {

		driver.findElement(By.cssSelector("#searchTextBox")).sendKeys("xbox");
		driver.findElement(By.cssSelector("#searchButton")).click();
		driver.findElement(By.cssSelector(".selectClub>a")).click();
		driver.findElement(By.cssSelector("#currentLocationAnchor>u")).click();
		
		WebDriverWait wait = new WebDriverWait(driver, 15);
		//wait.until(ExpectedConditions.alertIsPresent());
		
		 driver.switchTo().alert().dismiss();
	
				
				wait.until(ExpectedConditions.textToBePresentInElementLocated(By.xpath(".//*[@id='list']/ul/li[1]/div/div/span[1]/strong"), "CONCORD"));
		
		driver.findElement(By.xpath(".//*[@id='productList']/li[2]/span[1]/span/img")).click();
		boolean checkaddenable = driver.findElement(By.xpath(".//*[@id='prod14270704']/div[3]/div/a/div")).isEnabled();
		System.out.println(""+checkaddenable);
		
		//fail("Not yet implemented");
	}

}

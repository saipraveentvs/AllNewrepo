package samclubmweb;

import java.net.URL;

import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.uiautomation.ios.IOSCapabilities;

public class Iosdriver {
	
	public static void main(String[] args) throws Exception {
	    DesiredCapabilities safari = IOSCapabilities.iphone("Safari");
	    RemoteWebDriver driver = new RemoteWebDriver(new URL("http://localhost:5545/wd/hub"), safari);

	    driver.get("http://m.samsclub.com/");

	    System.out.println(driver.getTitle());
	    driver.quit();
	}

}

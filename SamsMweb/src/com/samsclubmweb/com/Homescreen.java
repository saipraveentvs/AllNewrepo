package com.samsclubmweb.com;

import static org.junit.Assert.*;

import java.io.FileInputStream;
import java.util.Properties;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

public class Homescreen {
	
	static WebDriver driver;

	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		
			DesiredCapabilities cap = new DesiredCapabilities();
			   cap.setCapability("deviceName", "iPhone Simulator");
		       cap.setCapability("browsername", "safari");
		       cap.setCapability("platformVersion", "7.1");
		       cap.setCapability("platformName", "iOS");
		     //Create Object of Properties Class.
				  Properties obj = new Properties();   
				  //Create Object of FileInputStream Class. Pass file path.
				  FileInputStream objfile = new FileInputStream(System.getProperty("user.dir")+"/src/samclubmweb/samsclub.properties");
				  //Pass object reference objfile to load method of Properties object.
				  obj.load(objfile); 
		       driver.get(obj.getProperty("URL"));

	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Test
	public void test() {
		fail("Not yet implemented");
	}

}

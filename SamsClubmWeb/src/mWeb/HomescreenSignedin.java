package mWeb;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.sql.Date;
import java.util.Calendar;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.sql.Date;

public class HomescreenSignedin {
	static WebDriver driver;
	FileInputStream objfile;
	 static Properties  obj = new Properties();
	
	
	public HomescreenSignedin() throws IOException{
		
		System.out.println("constructor");

		objfile = new FileInputStream(System.getProperty("user.dir")+"/src/samsclub.properties");
		obj.load(objfile); 
		  driver.get(obj.getProperty("URL_ENV"));
	}
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		DesiredCapabilities cap = new DesiredCapabilities();
		cap.setCapability("platformName", "ios");
		cap.setCapability("browser", "safari");
		cap.setCapability("deviceversion", "7.1");
		cap.setCapability("deviceName", "iphone simulator");

		driver = new RemoteWebDriver(new URL("http://localhost:4723/wd/hub"), cap);
		
		System.out.println("before end");
		
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		driver.quit();
		System.out.println("after end");
		System.out.println("Test compleated");
		
	}
	public String GetTimeStampValue()throws IOException{

        Calendar cal = Calendar.getInstance();       
         Date time=(Date) cal.getTime();
         String timestamp=time.toString();
            System.out.println(timestamp);
            String systime=timestamp.replace(":", "-");
            System.out.println(systime);
            return systime;
 
}

	@Test
	public void test() throws IOException {
		File screenshotFile=((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(screenshotFile,new File("//Users//vpsai"+obj+""+GetTimeStampValue()+".png"));
		
		
		try{
			WebDriverWait wait = new WebDriverWait(driver, 25);
			
	
		  wait.until(ExpectedConditions.textToBePresentInElementLocated(By.cssSelector(obj.getProperty("css_becomemember")), "Just $45 for a year of exclusive savings."));
			System.out.println("Test start1");
		driver.findElement(By.xpath(obj.getProperty("xp_signin"))).click();
		  wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("pLR10"), "Sign In to your Sam's Club account"));
		System.out.println("Test start2");
		driver.findElement(By.cssSelector(obj.getProperty("css_user"))).sendKeys(obj.getProperty("username"));
		System.out.println("Test start3");
		driver.findElement(By.cssSelector(obj.getProperty("css_password"))).sendKeys(obj.getProperty("password"));
	
		driver.findElement(By.cssSelector(obj.getProperty("css_signin_butt"))).click();
		wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("heading"), "Instant Savings*"));

			System.out.println("Test is pass");
		
		}
		
		catch(Exception e){
			System.out.println("Test is fail");
			System.out.println("exception"+e);
			
			
		}
			
			
		//wait.until(ExpectedConditions.textToBePresentInElementLocated(By.cssSelector(obj.getProperty("css_instsav")), "Instant Savings*"));
	}


}

package mWeb;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.sql.Date;
import java.util.Calendar;
import java.util.Properties;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.opera.core.systems.scope.protos.EcmascriptProtos.Object;

public class HomescreenGuest {
	
	static WebDriver driver;
	 static Properties obj = new Properties();

		static FileInputStream objfile; 

		
		
		public HomescreenGuest() throws IOException{

			try {
				System.out.println("constructor");
				objfile = new FileInputStream(System.getProperty("user.dir")+"/src/samsclub.properties");
				obj.load(objfile); 
				  driver.get(obj.getProperty("URL_ENV"));
				  
				
			} catch (FileNotFoundException e) {
				System.out.println("constructor exp");
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	
}
	

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		System.out.println("before class exp");
		DesiredCapabilities cap = new DesiredCapabilities();
		  cap.setCapability("deviceName", "iPhone Simulator");
	        cap.setCapability("browsername", "safari");
	        cap.setCapability("platformVersion", "7.1");
	        cap.setCapability("platformName", "iOS");
		
	
		driver = new RemoteWebDriver(new URL("http://localhost:4723/wd/hub"), cap);
		
	

	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		driver.quit();
		System.out.println("Test compleated");
		
	}

	@Test
	public void test() {

		try{
			System.out.println("Test start");
			WebDriverWait wait = new WebDriverWait(driver, 15);
			  wait.until(ExpectedConditions.textToBePresentInElementLocated(By.cssSelector(obj.getProperty("css_becomemember")), "Just $45 for a year of exclusive savings."));
			  
			  WebElement shopbycat = driver.findElement(By.xpath(obj.getProperty("xpath_shopbydep")));
			  System.out.println("Test shopbycat"+shopbycat);
			  boolean shopbycat_co = shopbycat.getSize() != null;
			  System.out.println("Test shopbycat_co"+shopbycat_co);
			 if(shopbycat_co == true)
			 {
				 
				 System.out.println("Test is Pass");
			 }
			 
			 else{
				 System.out.println("Test is Fail");
				 
			 }
			 
			
		}
		
		catch(Exception e){
			
			System.out.println("exception is"+e );
			driver.quit();
		}
		
	
		
		//fail("Not yet implemented");
	}
	
	public String GetTimeStampValue()throws IOException{

        Calendar cal = Calendar.getInstance();       
         Date time=(Date) cal.getTime();
         String timestamp=time.toString();
            System.out.println(timestamp);
            String systime=timestamp.replace(":", "-");
            System.out.println(systime);
            return systime;


 
}

}

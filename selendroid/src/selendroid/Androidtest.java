package selendroid;

import static org.junit.Assert.*;
import junit.framework.Assert;
import io.selendroid.SelendroidCapabilities;
import io.selendroid.SelendroidDriver;
import io.selendroid.device.DeviceTargetPlatform;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Androidtest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Test
	public void test() throws Exception {
		
		SelendroidCapabilities capa = new SelendroidCapabilities("io.selendroid.testapp:0.10.0");
		
		//SelendroidCapabilities capa = new SelendroidCapabilities("io.selendroid.testapp:0.8.0");
		capa.setPlatformVersion(DeviceTargetPlatform.ANDROID17);
		capa.setEmulator(false);

		WebDriver driver = new SelendroidDriver(capa);
		WebElement inputField = driver.findElement(By.id("my_text_field"));
		Assert.assertEquals("true", inputField.getAttribute("enabled"));
		inputField.sendKeys("Selendroid");
		Assert.assertEquals("Selendroid", inputField.getText());
		driver.quit();
		//fail("Not yet implemented");
	}

}

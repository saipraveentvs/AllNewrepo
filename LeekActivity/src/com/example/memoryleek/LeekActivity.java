package com.example.memoryleek;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.memoryleek.LeekManager.LeekListener;

public class LeekActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_leek);
		
		if (savedInstanceState == null) {
			getFragmentManager().beginTransaction().add(R.id.container, new LeekFragment()).commit();
		}
	}
	
	public static class LeekFragment extends Fragment implements LeekListener {

		private View leek;
		
		public LeekFragment() {
		}
		
		@Override
		public void onCreate(Bundle savedInstanceState) {
			super.onCreate(savedInstanceState);
			
			// Add this fragment as a leek listener so
			// it can update its view accordingly
			LeekManager.get().addListener(this);
		}
//		
//		@Override
//		public void onDestroy() {
//			super.onDestroy();
//			
//			LeekManager.get().removeListener(this);
//		}
		
		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_leek, container, false);
			
			leek = rootView.findViewById(R.id.leek);
			
			return rootView;
		}

		@Override
		public void onShowLeek(final boolean show) {
			Log.d("test", "Show leek? "+show);
			
			// need to update views on UI thread
			leek.post(new Runnable() {
				@Override
				public void run() {
					if (leek != null) {
						leek.setVisibility(show ? View.VISIBLE : View.INVISIBLE);
					}
				}
			});
		}
	}
}
